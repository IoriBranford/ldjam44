﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Limb : MonoBehaviour
{
	private Vector3 _throwDestination;

	public float _maxEnemyHits = 6;
	private float _enemyHits;

	private Sprite _intactSprite;
	public Sprite _damagedSprite;

	public float _gravitationToBody = 16;
	public Vector2 _attachOffset = Vector2.zero;

	public AudioClip[] _hitSounds;

	void Start()
	{
		_enemyHits = _maxEnemyHits;
		SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
		_intactSprite = spriteRenderer.sprite;
	}

	void OnMouseEnter()
	{
		if (necro)
			color = Color.red;
	}

	void OnMouseExit()
	{
		if (necro)
			color = Color.white;
	}

	void OnMouseDown()
	{
		if (necro) {
			necro.SelectLimb(this);
			color = Color.white;
		}
	}

	internal void Throw(float speed, float spin, Vector3 destination, bool destroyed)
	{
		color = Color.white;
		transform.SetParent(null);
		var throwvector = destination - transform.position;
		throwvector.Normalize();
		Rigidbody2D rigidbody = GetComponent<Rigidbody2D>();
		rigidbody.velocity = throwvector * speed;
		rigidbody.angularVelocity = spin;
		_throwDestination = destination;
		if (destroyed) {
			_enemyHits = 0;
			sprite = _damagedSprite;
		}
	}

	void FixedUpdate()
	{
		Rigidbody2D rigidbody = GetComponent<Rigidbody2D>();
		if (transform.parent) {
			var dir3 = transform.parent.position - transform.position;
			var dir = new Vector2(dir3.x, dir3.y) + _attachOffset;
			dir.Normalize();
			var targetvelocity = _gravitationToBody*dir;
			var acceleration = targetvelocity - rigidbody.velocity;
			rigidbody.AddForceAtPosition(acceleration, transform.position);
		} else {
			float sqrMagnitude = rigidbody.velocity.sqrMagnitude;
			if (sqrMagnitude > 0) {
				var throwvector3 = _throwDestination - transform.position;
				var throwvector = new Vector2(throwvector3.x, throwvector3.y);
				Vector2 move = rigidbody.velocity * Time.fixedDeltaTime;
				if (Vector2.Dot(throwvector, move) < move.magnitude) {
					transform.position = _throwDestination;
					rigidbody.velocity = Vector2.zero;
				}
			}
		}
	}

	void Update()
	{
		if (_enemyHits <= 0) {
			var mysprite = GetComponent<SpriteRenderer>();
			Color color = mysprite.color;
			color.a = Mathf.Max(0, color.a - Time.deltaTime);
			mysprite.color = color;
			if (color.a == 0) {
				Destroy(gameObject);
				return;
			}
		} else if (necro) {
			_enemyHits = Mathf.Min(_enemyHits + Time.deltaTime, _maxEnemyHits);
			if (_enemyHits >= 2) {
				sprite = _intactSprite;
			}
		}
	}

	void AttachTo(Necro newnecro)
	{
		Rigidbody2D rigidbody = GetComponent<Rigidbody2D>();
		rigidbody.angularVelocity = 0;
		color = Color.white;
		transform.SetParent(newnecro.transform);
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		Enemy myenemy = GetComponentInParent<Enemy>();
		Rigidbody2D rigidbody = GetComponent<Rigidbody2D>();
		if (collider.gameObject.name.Equals("MagicCircle")) {
			Necro newnecro = collider.transform.GetComponentInParent<Necro>();
			if (newnecro && !necro && !myenemy) {
				AttachTo(newnecro);
			}
		}

		Limb otherlimb = collider.gameObject.GetComponent<Limb>();
		Enemy otherenemy = collider.gameObject.GetComponent<Enemy>();
		if (!otherenemy)
			otherenemy = collider.gameObject.GetComponentInParent<Enemy>();

		if (otherenemy && otherenemy.IsAlive()) {
			if (necro) {
				otherenemy.CutLimb(this, necro._throwSpeed, 45);
				--_enemyHits;
				PlayRandomHitSound();
			} else if (_enemyHits > 0 && Mathf.Abs(rigidbody.angularVelocity) >= 720) {
				if (!otherlimb)
					otherlimb = otherenemy.GetComponentInChildren<Limb>();
				if (otherlimb) {
					otherenemy.CutLimb(otherlimb, Necro.instance._throwSpeed, 45);
					--_enemyHits;
					PlayRandomHitSound();
				}
			}
			if (_enemyHits < 2 && _damagedSprite) {
				sprite = _damagedSprite;
			}
		}
	}

	void PlayRandomHitSound()
	{
		if (_hitSounds != null && _hitSounds.Length > 0) {
			PlaySound(_hitSounds[Random.Range(0, _hitSounds.Length)]);
		}
	}
	void PlaySound(AudioClip clip)
	{
		AudioSource audioSource = GetComponent<AudioSource>();
		if (!audioSource || !clip)
			return;

		audioSource.PlayOneShot(clip);
	}

	internal Necro necro
	{
		get { return gameObject.GetComponentInParent<Necro>(); }
	}

	Sprite sprite
	{
		set
		{
			SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
			spriteRenderer.sprite = value;
		}
		get
		{
			SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
			return spriteRenderer.sprite;
		}
	}

	Color color
	{
		set
		{
			SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
			spriteRenderer.material.color = value;
		}
		get
		{
			SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
			return spriteRenderer.material.color;
		}
	}

}
