﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour
{
	public float _spawnTime = 4;
	public Enemy _enemyPrefab;
	private Transform[] _spawns;
	private bool _spawnsFinished;
	private int _numEnemies;

	void Start()
	{
		_spawns = GetComponentsInChildren<Transform>();
		_spawnsFinished = false;
		StartCoroutine("SpawnLoop");
	}

	// Update is called once per frame
	IEnumerator SpawnLoop()
	{
		while (_spawnTime > 0) {
			Transform spawn = _spawns[Random.Range(0, _spawns.Length)];
			Instantiate(_enemyPrefab, spawn);
			_spawnTime -= 0.125f;
			yield return new WaitForSeconds(_spawnTime);
		}
		_spawnsFinished = true;
	}

	internal bool AllCleared()
	{
		Enemy enemy = GetComponentInChildren<Enemy>();
		return _spawnsFinished && !enemy;
	}
}
