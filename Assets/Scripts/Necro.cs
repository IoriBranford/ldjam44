﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Necro : MonoBehaviour
{
	public int _limbsToWin = 10;
	public float _speed = 10;
	public float _throwSpeed = 50;
	public float _throwSpin = 1080;

	public float _deadFadeSpeed = 0.25f;
	public Sprite _deadHead;
	public Sprite _deadTorso;

	private Enemies _enemies;
	private GameObject _magicCircle;
	private Limb _selectedLimb;
	private LineRenderer _targetLine;

	private static Necro _instance = null;
	public static Necro instance
	{
		get { return _instance; }
	}

	private Image _resultImage;
	private GameObject _gameOverButtons;
	private AudioSource _music;

	public AudioClip[] _throwSounds;
	private int _numLimbs;
	public int numLimbs { get { return _numLimbs; } }

	void Start()
	{
		if (!_instance)
			_instance = this;

		GameObject lineobject = GameObject.Find("TargetLine");
		if (lineobject)
			_targetLine = lineobject.GetComponent<LineRenderer>();

		Transform magicCircle = transform.Find("MagicCircle");
		if (magicCircle) {
			_magicCircle = magicCircle.gameObject;
			_magicCircle.SetActive(false);
		}
		_resultImage = null;

		_enemies = GameObject.FindObjectOfType<Enemies>();
		GameObject music = GameObject.Find("Music");
		if (music)
			_music = music.GetComponent<AudioSource>();
		_gameOverButtons = GameObject.Find("/UI/GameOverButtons");
		if (_gameOverButtons) {
			_gameOverButtons.SetActive(false);
		}
	}

	internal void SelectLimb(Limb limb)
	{
		_selectedLimb = limb;
	}

	void OnMouseDown()
	{
		var limb = GetComponentInChildren<Limb>();
		if (limb)
			SelectLimb(limb);
	}

	Image FindResultImage(string name)
	{
		GameObject resultImage = GameObject.Find(name);
		if (resultImage)
			return resultImage.GetComponent<Image>();
		return null;
	}

	internal bool IsAlive()
	{
		var limb = GetComponentInChildren<Limb>();
		return limb != null;
	}

	void Update()
	{
		Rigidbody2D rigidbody = gameObject.GetComponent<Rigidbody2D>();

		var limbs = GetComponentsInChildren<Limb>();
		_numLimbs = limbs.Length;
		if (limbs.Length <= 0) {
			Color color;
			var sprites = GetComponentsInChildren<SpriteRenderer>();
			float deltaalpha = -Time.deltaTime*_deadFadeSpeed;
			foreach (SpriteRenderer sprite in sprites) {
				color = sprite.color;
				color.a = Mathf.Max(0, color.a + deltaalpha);
				sprite.color = color;
				if (_deadHead && sprite.transform.name == "Head") {
					sprite.sprite = _deadHead;
				}
			}
			var mysprite = GetComponent<SpriteRenderer>();
			color = mysprite.color;
			color.a = Mathf.Max(0, color.a + deltaalpha);
			mysprite.color = color;
			if (_deadTorso)
				mysprite.sprite = _deadTorso;
			rigidbody.velocity -= rigidbody.velocity*Time.deltaTime;
			if (_targetLine)
				_targetLine.enabled = false;
			if (_magicCircle)
				_magicCircle.SetActive(false);

			var particleSystem = GetComponentInChildren<ParticleSystem>();
			if (particleSystem) {
				if (color.a >= 0.5f)
					particleSystem.Emit(1);
			}

			if (color.a == 0) {
				if (!_resultImage)
					_resultImage = FindResultImage("Lose");
			} else {
				return;
			}
		} else if (limbs.Length >= _limbsToWin || _enemies && _enemies.AllCleared()) {
			if (!_resultImage)
				_resultImage = FindResultImage("Win");
		}

		if (_resultImage) {
			Color color = _resultImage.color;
			color.a = Mathf.Min(1, color.a + Time.deltaTime*_deadFadeSpeed);
			_resultImage.color = color;
			if (_music)
				_music.volume -= Time.deltaTime*_deadFadeSpeed;
			if (color.a >= 1)
				if (_gameOverButtons)
					_gameOverButtons.SetActive(true);
			return;
		}

		Vector2 moveInput = new Vector2(
				Input.GetAxis("Horizontal"),
				Input.GetAxis("Vertical"));
		rigidbody.velocity = moveInput*_speed;
		if (_magicCircle) {
			if (Input.GetButtonDown("Jump"))
				_magicCircle.SetActive(true);
			else if (Input.GetButtonUp("Jump"))
				_magicCircle.SetActive(false);
		}

		Camera camera = Camera.main;
		Vector2 mousescreenpos = Input.mousePosition;
		Vector2 mousepos = camera.ScreenToWorldPoint(mousescreenpos);
		if (_selectedLimb) {
			if (_targetLine) {
				_targetLine.enabled = true;
				_targetLine.SetPosition(0, _selectedLimb.transform.position);
				_targetLine.SetPosition(1, mousepos);
			}
			if (Input.GetMouseButtonUp(0)) {
				if (_throwSounds != null && _throwSounds.Length > 0) {
					PlaySound(_throwSounds[Random.Range(0, _throwSounds.Length)]);
				}
				_selectedLimb.Throw(_throwSpeed, _throwSpin, mousepos, false);
				_selectedLimb = null;
			}
		}

		if (!_selectedLimb) {
			if (_targetLine)
				_targetLine.enabled = false;
		}

		if (Input.GetKeyDown(KeyCode.Escape))
			SceneManager.LoadScene("Title");
	}

	void PlaySound(AudioClip clip)
	{
		AudioSource audioSource = GetComponent<AudioSource>();
		if (!audioSource || !clip)
			return;

		audioSource.PlayOneShot(clip);
	}

	void OnTriggerEnter2D(Collider2D collider) {
		Enemy enemy = collider.gameObject.GetComponent<Enemy>();
		if (!enemy)
			enemy = collider.gameObject.GetComponentInParent<Enemy>();
		if (enemy && enemy.IsAlive()) {
			var limb = GetComponentInChildren<Limb>();
			if (limb) {
				enemy.CutLimb(limb, _throwSpeed, 0);
			}
		}
	}
}
