﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	public float _destroyLimbChance = 0.9f;
	public float _speed = 5;

	public Sprite _deadHead;
	public Sprite _deadTorso;
	public float _deadFadeSpeed = 0.25f;

	public AudioClip _deadSound;

	void Start()
	{
	}

	internal bool IsAlive()
	{
		var limb = GetComponentInChildren<Limb>();
		return limb != null;
	}

	void FixedUpdate()
	{
		Necro necro = Necro.instance;
		var rigidbody = GetComponent<Rigidbody2D>();
		var targetvelocity = new Vector2();
		if (necro.IsAlive() && IsAlive()) {
			var dir = necro.transform.position - transform.position;
			dir.Normalize();
			var targetvelocity3 = _speed*dir;
			targetvelocity.x = targetvelocity3.x;
			targetvelocity.y = targetvelocity3.y;
		}

		rigidbody.velocity += (targetvelocity - rigidbody.velocity)*Time.fixedDeltaTime;
	}

	void Update()
	{
		if (!IsAlive()) {
			AudioSource audioSource = GetComponent<AudioSource>();
			if (audioSource && audioSource.clip != _deadSound)
				PlaySound(_deadSound);
			float deltaalpha = -Time.deltaTime*_deadFadeSpeed;
			Color color;
			var sprites = GetComponentsInChildren<SpriteRenderer>();
			foreach (SpriteRenderer sprite in sprites) {
				color = sprite.color;
				color.a = Mathf.Max(0, color.a + deltaalpha);
				sprite.color = color;
				if (_deadHead && sprite.transform.name == "Head") {
					sprite.sprite = _deadHead;
				}
			}
			var mysprite = GetComponent<SpriteRenderer>();
			color = mysprite.color;
			color.a = Mathf.Max(0, color.a + deltaalpha);
			mysprite.color = color;
			if (_deadTorso)
				mysprite.sprite = _deadTorso;

			var particleSystem = GetComponentInChildren<ParticleSystem>();
			if (particleSystem) {
				if (color.a >= 0.5f)
					particleSystem.Emit(1);
			}

			if (color.a == 0) {
				Destroy(gameObject);
				return;
			}
		}
	}

	//void OnTriggerEnter2D(Collider2D collider)
	//{
	//	Limb limb = collider.GetComponent<Limb>();
	//	if (limb && limb.necro) {
	//		CutLimb(limb, limb.necro._throwSpeed, limb.necro._throwSpin);
	//	}
	//}

	internal void CutLimb(Limb limb, float speed, float spin)
	{
		var angle = Random.Range(0, 360);
		var dist = Random.Range(1, 2);
		var dir = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle));
		var dest = limb.transform.position + dir*dist;
		var destroyed = false;
		var necro = Necro.instance;
		if (necro && necro.numLimbs > 1)
			destroyed = Random.value < _destroyLimbChance;
		limb.Throw(speed, spin, dest, destroyed);
		var rigidbody = GetComponent<Rigidbody2D>();
		rigidbody.velocity = -rigidbody.velocity;
	}

	void PlaySound(AudioClip clip)
	{
		AudioSource audioSource = GetComponent<AudioSource>();
		if (!audioSource || !clip)
			return;

		audioSource.PlayOneShot(clip);
	}

}
